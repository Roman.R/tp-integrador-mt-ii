using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1Controller : MonoBehaviour
{
    public float speed = 5f;
    public float rotationSpeed = 100f;
    public GameObject projectilePrefab;
    public Transform projectileSpawnPoint;
    public float projectileForce = 10f;
    public int maxHealth = 100;
    public int currentHealth;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        currentHealth = maxHealth;
    }

    private void Update()
    {
        float moveInput = Input.GetAxis("Vertical1");
        float rotateInput = Input.GetAxis("Horizontal1");

        // Movimiento hacia adelante/atr�s
        Vector3 movement = transform.forward * moveInput * speed * Time.deltaTime;
        rb.MovePosition(rb.position + movement);

        // Rotaci�n sobre el eje vertical
        Quaternion rotation = Quaternion.Euler(0f, rotateInput * rotationSpeed * Time.deltaTime, 0f);
        rb.MoveRotation(rb.rotation * rotation);

        // Disparo de proyectiles
        if (Input.GetButtonDown("Player1Fire"))
        {
            Fire();
        }
    }

    private void Fire()
    {
        GameObject projectile = Instantiate(projectilePrefab, projectileSpawnPoint.position, Quaternion.identity);
        Rigidbody projectileRb = projectile.GetComponent<Rigidbody>();
        projectileRb.AddForce(projectileSpawnPoint.forward * projectileForce, ForceMode.VelocityChange);
        Destroy(projectile, 2f);
    }

    private void Die()
    {
        // Acciones cuando el jugador muere
        Destroy(gameObject);
    }

    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        // Verificar si el jugador est� muerto
        if (currentHealth <= 0)
        {
            Die();
        }
    }

}
