using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int damageAmount = 10;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player2Controller player = other.GetComponent<Player2Controller>();
            player.TakeDamage(damageAmount);
            Destroy(gameObject);
        }
        //else if (other.CompareTag("Player2"))
        //{
        //    Player1Controller player = other.GetComponent<Player1Controller>();
        //    player.TakeDamage(damageAmount);
        //    Destroy(gameObject);
        //}
    }
}

